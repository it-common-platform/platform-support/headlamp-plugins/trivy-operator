import { registerRoute, registerSidebarEntry } from '@kinvolk/headlamp-plugin/lib';
import SbomReportDetail from './components/sbomReports/details';
import SbomReportList from './components/sbomReports/list';
import VulnerabilityReportDetail from './components/vulnerabilityReports/details';
import VulnerabilityReportList from './components/vulnerabilityReports/list';
import ExposedSecretReportDetail from './components/exposedSecretReports/details';
import ExposedSecretReportList from './components/exposedSecretReports/list';

// Main Trivy sidebar item
registerSidebarEntry({
  name: 'Trivy',
  url: '/trivy/vulnerabilityReports',
  icon: 'mdi:shield-bug',
  label: 'Trivy',
});

// ***** Vulnerabilty reports *****
registerSidebarEntry({
  name: 'VulnerabilityReports',
  url: '/trivy/vulnerabilityReports',
  icon: '',
  parent: 'Trivy',
  label: 'Vulnerability Reports',
});

registerRoute({
  path: '/trivy/vulnerabilityReports',
  sidebar: 'VulnerabilityReports',
  name: 'VulnerabilityReportList',
  exact: true,
  component: () => <VulnerabilityReportList />,
});

registerRoute({
  path: '/trivy/vulnerabilityReport/:vulnerabilityReportString',
  sidebar: 'VulnerabilityReports',
  name: 'Vulnerability Report Detail',
  exact: true,
  component: () => <VulnerabilityReportDetail />,
});

// ***** SBOM reports *****
registerSidebarEntry({
  name: 'SbomReports',
  url: '/trivy/sbomReports',
  icon: '',
  parent: 'Trivy',
  label: 'SBOM Reports',
});

registerRoute({
  path: '/trivy/sbomReports',
  sidebar: 'SbomReports',
  name: 'SbomReportList',
  exact: true,
  component: () => <SbomReportList />,
});

registerRoute({
  path: '/trivy/sbomReport/:sbomReportString',
  sidebar: 'SbomReports',
  name: 'SBOM Report Detail',
  exact: true,
  component: () => <SbomReportDetail />,
});

// ***** exposed Secret reports *****
registerSidebarEntry({
  name: 'ExposedSecretReports',
  url: '/trivy/exposedSecretReports',
  icon: '',
  parent: 'Trivy',
  label: 'Exposed Secret Reports',
});

registerRoute({
  path: '/trivy/exposedSecretReports',
  sidebar: 'ExposedSecretReports',
  name: 'ExposedSecretReportList',
  exact: true,
  component: () => <ExposedSecretReportList />,
});

registerRoute({
  path: '/trivy/exposedSecretReport/:exposedSecretReportString',
  sidebar: 'ExposedSecretReports',
  name: 'Exposed Secret Report Detail',
  exact: true,
  component: () => <ExposedSecretReportDetail />,
});
