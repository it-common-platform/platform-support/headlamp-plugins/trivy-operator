import { TextField } from '@mui/material';

const searchInObject = (obj, searchTerm) => {
  if (typeof obj === 'string' || typeof obj === 'number' || typeof obj === 'boolean') {
    return obj.toString().toLowerCase().includes(searchTerm.toLowerCase());
  } else if (Array.isArray(obj)) {
    return obj.some(item => searchInObject(item, searchTerm));
  } else if (typeof obj === 'object' && obj !== null) {
    return Object.values(obj).some(value => searchInObject(value, searchTerm));
  }
  return false;
};

export const dynamicFilterFunction = searchTerm => row => {
  return searchInObject(row, searchTerm);
};

export function Search({ searchTerm, setSearchTerm }) {
  return (
    <TextField
      style={{
        width: '20vw',
        margin: '0 1rem',
      }}
      id="outlined-basic"
      label="Search"
      value={searchTerm}
      // eslint-disable-next-line jsx-a11y/no-autofocus
      autoFocus
      onChange={event => {
        setSearchTerm(event.target.value);
      }}
    />
  );
}
