import { Link, SectionHeader, SimpleTable } from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { Box } from '@mui/material';
import { useEffect, useState } from 'react';
import { listSbomReports } from '../../api/sbomReports';
import { dynamicFilterFunction, Search } from '../searchUtils';

export default function SbomReportList({ fetchSbomReports = listSbomReports }) {
  const [sbomReports, setSbomReports] = useState<Array<any> | null>(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    fetchSbomReports().then(response => {
      if (!response.items) {
        setSbomReports([]);
        return;
      }
      const filterFunction = dynamicFilterFunction(searchTerm);
      const filteredResult = response.items.filter(filterFunction) ?? [];
      setSbomReports(filteredResult);
      //console.log('SBOM reports: ', filteredResult);
    });
  }, [searchTerm]);

  return (
    <>
      <SectionHeader
        title="SBOM Reports"
        actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
      />
      <SimpleTable
        columns={[
          {
            label: 'Name',
            sort: true,
            getter: sbomReport => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    routeName="/trivy/sbomReport/:sbomReportString"
                    params={{
                      sbomReportString: JSON.stringify(sbomReport),
                    }}
                  >
                    {sbomReport.metadata.name}
                  </Link>
                </Box>
              </Box>
            ),
          },
          {
            label: 'Namespace',
            sort: true,
            getter: sbomReport => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    routeName="namespace"
                    params={{
                      name: sbomReport.metadata.namespace,
                    }}
                  >
                    {sbomReport.metadata.namespace}
                  </Link>
                </Box>
              </Box>
            ),
          },
          {
            label: 'Registry',
            getter: sbomReport => sbomReport.report.registry.server,
            sort: true,
          },
          {
            label: 'Dependencies',
            getter: sbomReport => sbomReport.report.summary.dependenciesCount,
            sort: true,
          },
        ]}
        data={sbomReports}
      />
    </>
  );
}
