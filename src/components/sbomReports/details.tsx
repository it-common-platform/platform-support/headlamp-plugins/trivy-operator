import { Router } from '@kinvolk/headlamp-plugin/lib';
import {
  NameValueTable,
  SectionBox,
  SectionHeader,
  SimpleTable,
} from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { dynamicFilterFunction, Search } from '../searchUtils';

const { createRouteURL } = Router;

export default function SbomReportDetail() {
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredComponents, setFilteredComponents] = useState([]);

  const { sbomReportString } = useParams<{
    sbomReportString: string;
  }>();

  //console.log('Opening sbom report detail');

  if (!sbomReportString) {
    //console.log('No sbom report received');
    return <></>;
  }

  const sbomReport = JSON.parse(sbomReportString.replace(/%2F/g, '/'));
  //console.log('SBOM report: ', sbomReport);

  const sbomComponents = sbomReport.report.components.components;
  //console.log('SBOM Components: ', sbomComponents);

  useEffect(() => {
    const filterFunction = dynamicFilterFunction(searchTerm);
    setFilteredComponents(sbomComponents.filter(filterFunction) ?? []);
  }, [searchTerm]);

  return (
    <>
      {sbomReport && (
        <>
          <SectionBox
            backLink={createRouteURL('SbomReports')}
            title={<SectionHeader title={`${sbomReport.metadata.name}`} />}
          ></SectionBox>
          <SectionBox title={<SectionHeader title={`Artifact Details`} />}>
            <NameValueTable
              rows={[
                {
                  name: 'Digest',
                  value: sbomReport.report.artifact?.digest ?? 'Unknown',
                },
                {
                  name: 'Repository',
                  value: sbomReport.report.artifact?.repository ?? 'Unknown',
                },
                {
                  name: 'Tag',
                  value: sbomReport.report.artifact?.tag ?? 'Unknown',
                },
                {
                  name: 'Registry',
                  value: sbomReport.report.registry?.server ?? 'Unknown',
                },
                {
                  name: 'Dependencies',
                  value: sbomReport.report.summary.dependenciesCount ?? 'Unknown',
                },
              ]}
            />
          </SectionBox>
          <SectionHeader
            title="Components"
            actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
          />
          <SimpleTable
            columns={[
              {
                label: 'bom-ref',
                sort: true,
                gridTemplate: 0.8,
                getter: sbomComponent => sbomComponent['bom-ref'],
              },
              {
                label: 'Hashes',
                getter: sbomComponent => {
                  const hashes = [];
                  sbomComponent.hashes?.forEach(element => {
                    const hashString = `${element.alg ?? 'unknown'}:${
                      element.content ?? 'unknown'
                    }`;
                    hashes.push(hashString);
                    //console.log('Hash: ', hashString);
                  });
                  return hashes.join(', ') ?? '';
                },
                sort: true,
                gridTemplate: 0.5,
              },
              {
                label: 'Licenses',
                getter: sbomComponent => {
                  const licenses = [];
                  sbomComponent.licenses?.forEach(element => {
                    licenses.push(element.license?.name);
                    //console.log('Liscense: ', licenses);
                  });
                  return licenses.join(', ') ?? '';
                },
                sort: true,
                gridTemplate: 0.5,
              },
              {
                label: 'Type',
                sort: true,
                getter: sbomComponent => sbomComponent.type ?? '',
              },
              {
                label: 'Version',
                getter: sbomComponent => sbomComponent.version ?? '',
                gridTemplate: 0.5,
              },
            ]}
            data={filteredComponents ?? []}
          />
        </>
      )}
    </>
  );
}
