import { Router } from '@kinvolk/headlamp-plugin/lib';
import {
  NameValueTable,
  SectionBox,
  SectionHeader,
  SimpleTable,
} from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { Box, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { dynamicFilterFunction, Search } from '../searchUtils';

const { createRouteURL } = Router;

export default function ExposedSecretReportDetail() {
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredExposedSecret, setFilteredExposedSecret] = useState([]);

  const { exposedSecretReportString } = useParams<{
    exposedSecretReportString: string;
  }>();

  // console.log('Opening exposed secret report detail');

  // if (!exposedSecretReportString) {
  // console.log('No exposed secret report received');
  //   return <></>;
  // }

  const exposedSecretReport = JSON.parse(exposedSecretReportString.replace(/%2F/g, '/'));

  // console.log('Exposed Secret report received');
  // console.log(exposedSecretReport);

  useEffect(() => {
    const filterFunction = dynamicFilterFunction(searchTerm);
    setFilteredExposedSecret(exposedSecretReport.report ?? []);
  }, [searchTerm]);

  return (
    <>
      {exposedSecretReport && (
        <>
          <SectionBox
            backLink={createRouteURL('ExposedSecretReports')}
            title={<SectionHeader title={`${exposedSecretReport.metadata.name}`} />}
          ></SectionBox>
          <SectionBox title={<SectionHeader title={`Artifact Details`} />}>
            <NameValueTable
              rows={[
                {
                  name: 'Digest',
                  value: exposedSecretReport.report.artifact?.digest ?? 'Unknown',
                },
                {
                  name: 'Repository',
                  value: exposedSecretReport.report.artifact?.repository ?? 'Unknown',
                },
                {
                  name: 'Tag',
                  value: exposedSecretReport.report.artifact?.tag ?? 'Unknown',
                },
                {
                  name: 'Registry',
                  value: exposedSecretReport.report.registry?.server ?? 'Unknown',
                },
              ]}
            />
          </SectionBox>
          <SectionHeader
            title="Exposed Secrets"
            actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
          />
          <SimpleTable
            columns={[
              {
                label: 'Category',
                getter: exposedsecret => exposedsecret.severity ?? 'Unknown',
                sort: true,
                gridTemplate: 0.5,
              },
              {
                label: 'CVSS Score',
                getter: exposedsecret => exposedsecret.score ?? 'Unknown',
                sort: true,
                gridTemplate: 0.5,
              },
              {
                label: 'Resource',
                getter: exposedsecret => exposedsecret.resource ?? 'Unknown',
              },
              {
                label: 'Installed Version',
                getter: exposedsecret => exposedsecret.installedVersion ?? 'Unknown',
                gridTemplate: 0.5,
              },
              {
                label: 'Fixed Version',
                getter: exposedsecret => exposedsecret.fixedVersion ?? 'Unknown',
                gridTemplate: 0.5,
              },
              {
                label: 'Description',
                getter: exposedsecret => exposedsecret.title ?? 'Unknown',
                gridTemplate: 2,
              },
            ]}
            data={filteredExposedSecret ?? []}
          />
        </>
      )}
    </>
  );
}
