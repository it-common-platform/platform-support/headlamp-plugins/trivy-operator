import { Link, SectionHeader, SimpleTable } from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { Box } from '@mui/material';
import { useEffect, useState } from 'react';
import React from 'react';
import { listExposedSecretReports } from '../../api/exposedSecretReports';
import { dynamicFilterFunction, Search } from '../searchUtils';

export default function ExposedSecretReportList({
  fetchExposedSecretReports = listExposedSecretReports,
}) {
  const [exposedSecretReports, setExposedSecretReports] = useState<Array<any> | null>(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    fetchExposedSecretReports().then(response => {
      if (!response.items) {
        console.log('Reports: ' + JSON.stringify(response[0]));
        if (!Array.isArray(response)) {
          setExposedSecretReports([]);
          return;
        }
      }
      const filterFunction = dynamicFilterFunction(searchTerm);
      const filteredResult =
        response.items.filter(filterFunction) ?? response.filter(filterFunction);
      setExposedSecretReports(filteredResult);
      console.log('Exposed secret reports: ', filteredResult);
    });
  }, [searchTerm]);
  return (
    <>
      <SectionHeader
        title="Exposed Secret Reports"
        actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
      />
      <SimpleTable
        columns={[
          {
            label: 'Name',
            sort: true,
            getter: exposedSecretReport => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    // routeName="/trivy/vulnerabilityReport/:vulnerabilityReportString"
                    routeName="/trivy/exposedSecretReport/:exposedSecretReportString"
                    params={{
                      exposedSecretReportString: JSON.stringify(exposedSecretReport),
                    }}
                  >
                    {exposedSecretReport.metadata.name}
                  </Link>
                </Box>
              </Box>
            ),
          },
          {
            label: 'Namespace',
            sort: true,
            getter: exposedSecretReport => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    routeName="namespace"
                    params={{
                      name: exposedSecretReport.metadata.namespace,
                    }}
                  >
                    {exposedSecretReport.metadata.namespace}
                  </Link>
                </Box>
              </Box>
            ),
          },
          {
            label: 'Report Name',
            getter: exposedSecretReport => exposedSecretReport.report.scanner.name,
            sort: true,
          },
          {
            label: 'Critical',
            getter: exposedSecretReport => exposedSecretReport.report.summary.criticalCount,
            sort: true,
          },
          {
            label: 'High',
            getter: exposedSecretReport => exposedSecretReport.report.summary.highCount,
            sort: true,
          },
          {
            label: 'Medium',
            getter: exposedSecretReport => exposedSecretReport.report.summary.mediumCount,
            sort: true,
          },
          {
            label: 'Low',
            getter: exposedSecretReport => exposedSecretReport.report.summary.lowCount,
            sort: true,
          },
        ]}
        data={exposedSecretReports}
      />
    </>
  );
}
