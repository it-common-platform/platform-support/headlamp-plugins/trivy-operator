import { ApiProxy } from '@kinvolk/headlamp-plugin/lib';
import { mapNamespaces, namespaces } from './common';

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  'X-HEADLAMP_BACKEND-TOKEN': new URLSearchParams(window.location.search).get('backendToken'),
});

export function listSbomReports() {
  if (namespaces.length > 0) {
    return mapNamespaces(getSbomReportsByNamespace);
  }

  return request('/apis/aquasecurity.github.io/v1alpha1/sbomreports', {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function getSbomReportsByNamespace(namespace: string) {
  return request(`/apis/aquasecurity.github.io/v1alpha1/namespaces/${namespace}/sbomreports`, {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function getSbomReport(namespace: string, reportName: string) {
  return request(
    `/apis/aquasecurity.github.io/v1alpha1/namespaces/${namespace}/sbomreports?name=${reportName}`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}
