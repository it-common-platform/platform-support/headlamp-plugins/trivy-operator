export const namespaces =
  JSON.parse(localStorage.getItem(`cluster_settings.${'main'}`) || '{}')?.allowedNamespaces || [];

export async function mapNamespaces(callback) {
  const response = namespaces.map(async element => {
    return callback(element);
  });
  const reportsPromises = Promise.all(response);
  const mergedResponse = {
    items: (await reportsPromises).reduce((acc, response) => acc.concat(response.items), []),
  };

  //Wait for all promises to resolve
  return mergedResponse;
}
