import { ApiProxy } from '@kinvolk/headlamp-plugin/lib';
import { mapNamespaces, namespaces } from './common';

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  'X-HEADLAMP_BACKEND-TOKEN': new URLSearchParams(window.location.search).get('backendToken'),
});

export async function listExposedSecretReports() {
  if (namespaces.length > 0) {
    return mapNamespaces(getExposedSecretReportsByNamespace);
  }

  return request('/apis/aquasecurity.github.io/v1alpha1/exposedsecretreports', {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function getExposedSecretReportsByNamespace(namespace: string) {
  return request(
    `/apis/aquasecurity.github.io/v1alpha1/namespaces/${namespace}/exposedsecretreports`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}

export function getExposedSecretReport(namespace: string, reportName: string) {
  return request(
    `/apis/aquasecurity.github.io/v1alpha1/namespaces/${namespace}/exposedsecretreports?name=${reportName}`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}
